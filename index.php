<?php
/**
 * @file
 * php-simple-upload - simple script to upload files to the web server
 *
 * Copyright (C) 2017  Antonio Ospite <ao2@ao2.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require __DIR__ . '/vendor/autoload.php';

use Sirius\Upload\Handler as UploadHandler;

// Path relative to the script dir.
const INCOMING_DIR = 'incoming/';

const MAX_FILE_SIZE = '1G';

const ALLOWED_EXTENSIONS = [
  'avi',
  'bz2',
  'gz',
  'htm',
  'html',
  'jpg',
  'mp3',
  'mpg',
  'php',
  'png',
  'rar',
  'txt',
  'zip',
];

if (isset($_POST['task']) && $_POST['task'] == "upload") {
  $uploadHandler = new UploadHandler(INCOMING_DIR);

  $uploadHandler->addRule('extension', ['allowed' => ALLOWED_EXTENSIONS], '{label} invalid file type', 'File');
  $uploadHandler->addRule('size', ['max' => MAX_FILE_SIZE], '{label} should be less than {max}', 'File');

  $result = $uploadHandler->process($_FILES);
  if ($result->isValid()) {
    try {
      $result->confirm();
    }
    catch (\Exception $e) {
      $result->clear();
      throw $e;
    }
  }
  else {
    echo "<pre>{$result->getMessages()}</pre>";
  }
}

$iframe_parent_request_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . INCOMING_DIR;

// Avoid iframe recursion.
if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] == $iframe_parent_request_url) {
  echo "Iframe recursion detected, use the BACK button in the browser";
  return;
}
?>

<h1>Upload</h1>
<form method="POST" enctype="multipart/form-data">
  <input type="file" name="filefield[]" multiple="true"/>
  <input type="hidden" name="task" value="upload"/>
  <input type="submit" value="Upload File"/>
</form>

<iframe src="<?php echo INCOMING_DIR; ?>" height="100%" width="100%" frameborder="0">
  Your browser does not support iframes <a href="<?php echo INCOMING_DIR; ?>">click here to view the page directly.</a>
</iframe>
